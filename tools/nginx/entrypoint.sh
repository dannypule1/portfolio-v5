#!/bin/sh

/usr/local/bin/envsubst < ${APP}/tools/javascript/config-template.js.conf > ${APP}/${ROOT_PATH}/config.js

APP=/var/src/app/
NGINX_CONF_DIR="${APP}/tools/nginx/"
ROOT_PATH=build


export ROOT_PATH
/usr/local/bin/envsubst '$ROOT_PATH' < ${NGINX_CONF_DIR}/nginx-template.conf > ${NGINX_CONF_DIR}/nginx.conf


if [ ! -z "$DEBUG" ]; then
   echo ">> dumping running nginx configuration"
   /bin/cat ${NGINX_CONF_DIR}/nginx.conf
   echo ">> dumping config of Bundle"
   /bin/cat ${APP}/${ROOT_PATH}/config.js
fi

echo ">> starting nginx..."

exec nginx -g "daemon off;" -c ${NGINX_CONF_DIR}/nginx.conf