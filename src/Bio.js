import React from 'react';
import avatar from './static/images/danny_pule.jpg';

const Bio = () => (
  <div>
    <div className="bio">
      <img
        src={avatar}
        alt="Danny Pule - Javascript Developer"
      />
      <h2>
        <span>Danny Pule</span>
      </h2>
      <h5>Javascript Developer based in South West London, UK</h5>
    </div>
    <div className="site-section how-i-do">
      <div className="section-inner">
        <h1>~ About ~</h1>
        <p>
          When I'm not building something or learning about some new web
          technology you can find me scrolling through Dribbble.com appreciating
          the designs contained therein.
        </p>
      </div>
    </div>
    <div className="site-section skillz">
      <div className="section-inner">
        <div>
          <h1>~ Tech I Work With ~</h1>
          <p>React, Redux, Webpack,</p>
          <p>HTML, CSS, SCSS, CSS-in-JS,</p>
          <p>Angular 2+, Typescript,</p>
          <p>Rx/JS, Modern Javascript,</p>
          <p>Node.js, Docker, CI/CD</p>
        </div>
      </div>
    </div>
  </div>
);

export default Bio;