import React from 'react';
import './App.scss';
import Projects from './Projects';
import Codepens from './Codepens';
import Footer from './Footer';
import Bio from './Bio';

const Home = () => (
  <div className="Home">
    <Bio />
    <Projects />
    <Codepens />
    <Footer />
  </div>
);

export default Home;