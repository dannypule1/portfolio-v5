import React from 'react';

const Footer = () => (
  <div className="credits-section">
    <span>©</span>
    <span>{new Date().getFullYear()}</span>
  </div>
)

export default Footer
